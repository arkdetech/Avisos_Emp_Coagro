# Avisos_Emp_Coagro

App para android, desarrollada en kodular, que le permite al operador reproducir alertas sonoras en el altavoz, alertando de la falla en las líneas de empaque de producto en Coagronorte Ltda.

Esta aplicacion permite desde una tablet, generar alertas sonoras a los operarios del area de empaquetado de Coagronorte, cuando se presenten errores en el empaquetado de producto.

Se compone basicamente de la "screen1", la pantalla de presentacion con el logo de la empresa, automaticamente pasa a la segunda pantalla, "Botonera", donde se encuentran los botones con avisos sonoros.

En la pantalla Botonera, se encuentran distribuidos dos columnas los botones, de izquierda a derecha, la primera columna podemos seleccionar cualquiera de las tres lineas de produccion, posteriormente en la segunda, seleccionamos el boton de acuerdo al proceso que se desea generar el aviso. 

Al final de la colunmna izquierda esta el aviso de llamado al cargador.
